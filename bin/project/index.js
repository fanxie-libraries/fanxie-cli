const fs = require('fs');
const chalk = require("chalk");
const inquirer = require("inquirer");
const shell = require('shelljs');

const { getCleanName, updatePackageFile, getProjectPackage } = require('../helpers');

const repoUrlPattern = 'https://gitlab.com/fanxie-kits/{PROJECT_TYPE}.git';

module.exports = {
    build_repo_url( project_type ) {
        const regex = /{PROJECT_TYPE}/gi;
        return repoUrlPattern.replace(regex, project_type);
    },
    /**
     *
     */
    create_blank(answers) {
        shell.exec(`git init ${answers.directory} -q`, {silent: true});
        shell.cd(answers.directory);

        updatePackageFile(`${shell.pwd()}/package.json`, {name: answers.name_clean, desc: answers.project_description});
        this.setup();

        //  TODO Make laravel-mix stubs so this is actually useful.
        // inquirer.prompt([
        //     {
        //         type: 'confirm',
        //         name: 'use_laravel_mix',
        //         message: 'Do you need/want Laravel Mix?',
        //     }
        // ]).then(answers => {
        //     if ( answers.use_laravel_mix ) {
        //         console.log(
        //             chalk.hex('#2affab')(`\nInstalling dependencies, this may take a few minutes...`)
        //         );
        //
        //         shell.exec('npm i -s laravel-mix', {silent: true});
        //
        //         console.log(
        //             chalk.hex('#2affab')(`\nDone.`)
        //         );
        //     }
        // })
    },
    /**
     *
     * @param type
     */
    create(type) {
        console.log(
            chalk.yellow(`Create new ${type} project`)
        );
        inquirer
            .prompt([
                {
                    type: "input",
                    name: "project_name",
                    message: "Project name:",
                    validate: (value) => {
                        return value.length > 0 ? true : "Project name can't be empty.";
                    }
                },
                {
                    type: "input",
                    name: "project_description",
                    message: "Project description:",
                    default: "Just a cool new project"
                },
                {
                    type: "input",
                    name: "directory",
                    message: "Directory name:",
                    default: (answers) => {
                        return getCleanName(answers.project_name);
                    }
                }
            ])
            .then(answers => {
                const project_name = answers.project_name;
                const project_name_clean = getCleanName(project_name);
                const repo_url = this.build_repo_url(type);

                if ( type === 'blank' ) {
                    this.create_blank({ ...answers, name_clean: project_name_clean });
                } else {
                    console.log(
                        chalk.hex('#2affab')(`\nCloning template from ${repo_url}...`)
                    );

                    let cloneAttempt = shell.exec(`git clone ${repo_url} ${answers.directory} -q`, {silent: true});

                    if ( cloneAttempt.code === 0 ) {
                        shell.cd(answers.directory);
                        shell.exec('git remote rm origin');
                        updatePackageFile(`${shell.pwd()}/package.json`, {name: project_name_clean, desc: answers.project_description});
                        this.setup();
                    } else {
                        console.log(
                            chalk.hex('#e06b82')(`\nERR: Could not find template on ${repo_url}.\nMake sure the repository exists.`)
                        );

                        inquirer.prompt([
                            {
                                type: 'confirm',
                                name: 'create_blank_instead',
                                message: 'Create blank project instead?'
                            }
                        ]).then(a => {
                            if ( a.create_blank_instead )
                                this.create_blank({ ...answers, name_clean: project_name_clean });
                        });
                    }
                }
            });

    },
    setup() {
        try {
            const config = require(`${shell.pwd()}/fanxie.setup.js`);
            console.log(
                chalk.hex('#2affab')(`\nRunning project setup...`)
            );
            config({ chalk, inquirer, shell, package: getProjectPackage( shell.pwd() ) })
                .then( commands => {
                    inquirer
                        .prompt([
                            {
                                type: 'confirm',
                                name: 'delete_setup_file',
                                message: 'Delete fanxie.setup.js file? (Yes)',
                                default: true
                            }
                        ]).then(answers => {
                            if ( answers.delete_setup_file )
                                fs.unlinkSync(`${shell.pwd()}/fanxie.setup.js`);

                            if ( commands ) {
                                for ( let i = 0; i < commands.length; i++ ) {
                                    console.log(commands[i]);
                                }
                            }
                        })
                })
        } catch (e) {
            console.log(
                chalk.hex('#efc100')(`\nFanxie setup file not found, skipping setup.`)
            );
        }
    }
};
