#!/usr/bin/env node
const path = require("path");

const inquirer = require("inquirer");
const chalk = require("chalk");
const figlet = require("figlet");

const program = require('commander');

const project = require('./project');

const { getProjectPackage } =  require('./helpers');

program
    .version(
        getProjectPackage(path.join(__dirname, '../')).get('version')
        , '-v, --version')
    .description('Fanxie Lab CLI utility');

program
    .command('new [type]')
    .alias('n')
    .description('Create new project')
    .action(type => {
        console.log(
            chalk.hex('#2affab')(figlet.textSync("FANXIE LAB"))
        );

        if (type !== undefined) {
            project.create(type);
        } else {
            inquirer
                .prompt([
                    {
                        input: "list",
                        name: "project_type",
                        message: "What are you building?",
                        default: "blank"
                    }
                ])
                .then(answer => {
                    project.create(answer.project_type);
                });
        }
    });

program
    .command('setup')
    .alias('s')
    .description('Run Fanxie setup for current project')
    .action( () => {
        project.setup();
    });


program.parse(process.argv);
