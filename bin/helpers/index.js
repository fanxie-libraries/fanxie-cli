const fs = require('fs');
const editJsonFile = require("edit-json-file");

const removeSpecialChars = function(string) {
    return string.replace(/[^a-zA-Z0-9]/g,'-');
};

module.exports = {
    /**
     *
     * @param name
     */
    createDirectory(name) {
        if (!fs.existsSync(name)){
            fs.mkdirSync(name);
        }
    },
    /**
     *
     * @param path
     * @param data
     */
    updatePackageFile(path, data) {
        let file = editJsonFile(path);
        file.set("name", data.name);
        file.set("description", data.desc);
        file.save();
    },
    getProjectPackage(path) {
        return editJsonFile(`${path}/package.json`);
    },
    /**
     *
     * @param name
     * @returns {string}
     */
    getCleanName(name) {
        return removeSpecialChars(name).toLowerCase()
    },
};
